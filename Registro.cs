﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen
{
    class Registro
    {
        private String nombres;

        public String Nombres
        {
            get { return nombres; }
            set { nombres = value; }
        }

        private String apellido;

        public String Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private String cedula;

        public String Cedula
        {
            get { return cedula; }
            set { cedula = value; }
        }
        private String Email;

        public String email
        {
            get { return Email; }
            set { Email = value; }
        }
        private String Contraseña;

        public String contraseña
        {
            get { return Contraseña; }
            set { Contraseña = value; }
        }
        public Registro(string nombres, string apellidos,String cedula, string Email, string Contraseña)
        {
            this.Nombres = nombres;
            this.Apellido = apellido;
            this.Cedula = cedula;
            this.email = Email;
            this.contraseña = Contraseña;
        }
    }
}
